<?php

namespace App\Http\Controllers;

use App\Models\RestTable;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    public function index()
    {

        $user = auth()->user();
        if ($user->isSuperAdmin() || empty(array_intersect(
                    $user->roles()->pluck('name')->toArray(),
                    ['restaurant']
              ))) {
            return redirect()->route('admin.dashboard');
        } elseif (in_array('restaurant', $user->roles()->pluck('name')->toArray())) {
            return redirect()->route('restaurant.dashboard');
        }
    }

    public function payPage($company_id, $table_id)
    {
        $section = $table->section?->name ?? 'SINGLE';
        $table = RestTable::findOrFail($table_id);
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'http://93.112.19.149:7848/DynamicsNAV110/api/beta/companies(64532d77-be7e-43d0-afcc-1ece296370fb)/ptreceiptNos?$filter=Sales_Type eq "SINGLE" and Table_No eq 30', [
              'auth' => ['administrator', 'p@ssw0rd@@net000', 'ntlm']
        ]);
        dd($response->getBody());
    }
}
