<?php

namespace App\Http\Controllers\Restaurant;

use App\DataTables\Restaurant\TableDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Restaurant\TableRequest;
use App\Models\Branch;
use App\Models\RestTable;
use App\Models\Section;
use App\Services\GenerateQrCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TablesController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:restaurant.tables.index')->only(['index']);
        $this->middleware('permission:restaurant.tables.store')->only(['store']);
        $this->middleware('permission:restaurant.tables.show')->only(['show']);
        $this->middleware('permission:restaurant.tables.update')->only(['update']);
        $this->middleware('permission:restaurant.tables.delete')->only(['delete']);

    }

    public function index(TableDataTable $tableDataTable)
    {
        return $tableDataTable->render('restaurants.tables.index');
    }


    public function create()
    {
        $branches = Branch::where('user_id', auth()->id())->pluck('name', 'id');
        $sections = Section::where('user_id', auth()->id())->pluck('name', 'id');
        return view('restaurants.tables.create', compact('branches', 'sections'));
    }


    public function store(TableRequest $request, RestTable $table)
    {
        $table->fill($request->validated() + ['user_id' => auth()->id()])->save();
        $company = auth()->user()->company_id ?? '64532d77-be7e-43d0-afcc-1ece296370fb';
        $value = url('/') . '/qr_code/' . $company . '/' . $table->id;
        $qr_code = GenerateQrCode::createQr($value, 'app/public/images/qrCodes/');
        $table->update(['qr_code' => $qr_code]);
        return redirect()->route('restaurant.tables.index')->with('success', trans('created_successfully'));
    }

    public function qrCode(RestTable $table)
    {

        return view('restaurants.tables.qrCode', compact('table'));
    }

    public function show(RestTable $table)
    {
        return view('restaurants.tables.show', compact('table'));
    }


    public function edit(RestTable $table)
    {
        $branches = Branch::where('user_id', auth()->id())->pluck('name', 'id');
        $sections = Section::where('user_id', auth()->id())->pluck('name', 'id');
        return view('restaurants.tables.edit', compact('branches', 'sections', 'table'));
    }


    public function update(TableRequest $request, RestTable $table)
    {
        $table->fill($request->validated() + ['user_id' => auth()->id()]);
        if (file_exists(public_path('/storage/' . $table->qr_code)))
            unlink(public_path('/storage/' . $table->qr_code));
        $company = auth()->user()->company_id ?? '64532d77-be7e-43d0-afcc-1ece296370fb';
        $value = url('/') . '/qr_code/' . $company . '/' . $table->id;
        $qr_code = GenerateQrCode::createQr($value, 'app/public/images/qrCodes/');
        $table->update(['qr_code' => $qr_code]);
        return redirect()->route('restaurant.tables.index')->with('success', trans('updated_successfully'));
    }


    public function destroy(RestTable $table)
    {
        if (file_exists(public_path('/storage/' . $table->qr_code)))
            unlink(public_path('/storage/' . $table->qr_code));
        $table->delete();
        return redirect()->route('restaurant.tables.index')->with('success', trans('deleted_successfully'));

    }

    public function getSections(Request $request)
    {
        $data['sections'] = Section::where('branch_id', $request->branch_id)->pluck('name', 'id');
        return response()->json($data['sections']);

    }
}
