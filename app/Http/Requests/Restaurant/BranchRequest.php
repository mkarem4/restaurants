<?php

namespace App\Http\Requests\Restaurant;

use Illuminate\Foundation\Http\FormRequest;

class BranchRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules= [
              'name'=>'required|string',
        ];
        if (!$this->isMethod('PUT')) {
            $rules['logo'] = 'required|image|max:10000';
        }else{
            $rules['logo'] = 'nullable|image|max:10000';

        }
        return $rules;
    }
}
