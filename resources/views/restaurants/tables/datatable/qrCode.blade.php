<button type="button"  data-toggle="modal" data-target="#exampleModal" style="border: none;background: none;">
  <img src="/storage/{{$query->qr_code}}" width=200 style='width: 110px;height: 54px;border-radius: unset;'></button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">@lang('qr_code')</h5>
      </div>
      <div class="modal-body">
          <img src="/storage/{{$query->qr_code}}" class="card-img-top" alt="..." id="print_this"  style="width: 250px;
            height: 200px;
            border-radius: unset;">
          <div class="card-body" style="padding: 1.5rem 0.5rem;">
            <form>
              <button type="button" class="btn btn-success m-2" id="print"
                      style="{{ (app()->getLocale() =='ar') ? 'left:0;' : 'right:0;' }}">@lang('print')</button>
            </form>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('close')</button>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('js/custom.js') }}"></script>
