<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Restaurant\HomeController;
use App\Http\Controllers\Restaurant\CustomerController;
use App\Http\Controllers\Restaurant\SectionController;
use App\Http\Controllers\Restaurant\branchController;
use App\Http\Controllers\Restaurant\TablesController;




Route::get('dashboard',[HomeController::class,'home'])->name('dashboard');
Route::resource('customers',CustomerController::class)->except('create','store');
Route::resource('branches',branchController::class);
Route::resource('sections',SectionController::class);
Route::resource('tables',TablesController::class);
Route::get('getSections',[TablesController::class,'getSections'])->name('getSections');
Route::get('tables/qrCode/{table}',[TablesController::class,'qrCode'])->name('tables.qrCode');
